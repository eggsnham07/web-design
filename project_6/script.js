/*
 * Date Compiled: 18/11/2022
 * Version: 1.1
*/
const System = {
    /**
     * This is the fancy printing
     * @param {String} msg 
     * @param {HTMLElement} stdout 
     * @returns 
     */
    println: async (msg, stdout, options) => {
        return new Promise((resolve) => {
            if(stdout == undefined) {
                stdout = document.getElementById("stdout")
                if(!prod) console.warn("stdout was not set, this is dangerous when document.getElementById(\"stdout\") is null")
            }
            if(msg === undefined) {
                if(!prod) System.println("[ERROR] Message was undefined", stdout)
                if(!prod) console.warn("msg was not set!")
            }

            if(env.PRINTING_RN) setTimeout(() => System.println(msg, stdout).then(resolve()), 300)
            else {
                var ms = options?.ms || 300
                var i = 0

                if(!prod) console.log(msg)
                if(!prod) console.log(options)

                switch(msg.replace("[INFO] ", '').replace("[WARN] ", '').replace("[ERROR] ", '')) {
                    case enums.flags.EMPTYSTR:
                        // This will just print newline
                        System.println(" ", stdout, {ms: 0})
                        resolve()
                        break
                    case enums.flags.CLEAR:
                        // Clear stdout and do not print
                        stdout.innerHTML = ''
                        resolve()
                        break
                    default:
                        // If none of the above match then do a normal println
                        env.PRINTING_RN = true

                        var interval = setInterval(() => {
                            if(msg[i] == "\n") stdout.appendChild(document.createElement("br"))
                            else stdout.innerHTML += msg[i]

                            stdout.scrollTop = stdout.scrollHeight
                            if(i == msg.length-1) {
                                window.clearInterval(interval)
                                env.PRINTING_RN = false
                                stdout.appendChild(document.createElement("br"))
                                resolve()
                            }

                            i++
                        }, ms/msg.length)
                }    
            }
        })
    }
}
